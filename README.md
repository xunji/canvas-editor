<h1 align="center">canvas-editor画布编辑器（富文本编辑器）</h1>

<p align="center">
<a href="https://www.npmjs.com/package/@hufe921/canvas-editor"><img src="https://img.shields.io/npm/v/@hufe921/canvas-editor.svg?sanitize=true" alt="Version"></a>
<a href="https://www.npmjs.com/package/@hufe921/canvas-editor"><img src="https://img.shields.io/npm/l/@hufe921/canvas-editor.svg?sanitize=true" alt="License"></a>
<a href="https://gitcode.com/Hufe921/canvas-editor/issues/new/choose"><img src="https://img.shields.io/badge/PRs-welcome-brightgreen.svg" alt="PRs"></a>
</p>

<p align="center">一款基于画布与SVG的富文本编辑器</p>

## tips

1. [文档](https://hufe.club/canvas-editor-docs/)
2. [canvas-editor-plugin](https://gitcode.com/gh_mirrors/ca/canvas-editor/overview)
3. 使用SVG作为渲染层的功能正在开发中，参见feature/svg [feature/svg](https://gitcode.com/Hufe921/canvas-editor/tree/feature/svg)
4. 现已支持导出PDF功能，参见feature/pdf [feature/pdf](https://gitcode.com/Hufe921/canvas-editor/tree/feature/pdf)

## usage

```bash
npm i @hufe921/canvas-editor --save
```

```html
<div class="canvas-editor"></div>
```

```javascript
import Editor from '@hufe921/canvas-editor'

new Editor(document.querySelector('.canvas-editor'), {
  main: [
    {
      value: 'Hello World'
    }
  ]
})
```

## next features

1. table paging
2. improve performance
3. control rules
4. [CRDT](https://gitcode.com/Hufe921/canvas-editor/tree/feature/CRDT)

## snapshot

![image](https://gitcode.com/Hufe921/canvas-editor/blob/main/src/assets/snapshots/main_v0.9.35.png)

## install

`yarn`

## dev

`npm run dev`

## build

#### app

`npm run build`

#### lib

`npm run lib`
