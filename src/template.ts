import {
  ControlType,
  ElementType,
  IEditorOption,
  IElement,
  RowFlex,
  TitleLevel
} from './editor'
import form from './data.json'
const elementList: IElement[] = [
  {
    value: '              ',
    size: 16,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false,
    rowFlex: RowFlex.RIGHT
  },
  {
    value: '',
    type: ElementType.TITLE,
    level: TitleLevel.FIRST, //标题级别
    valueList: [
      {
        value: ' \n合同编号： ',
        size: 17,
        bold: false,
        color: 'rgb(102, 102, 102)',
        italic: false,
        rowFlex: RowFlex.RIGHT
      }
    ]
  },
  {
    value: '',
    type: ElementType.CONTROL,
    rowFlex: RowFlex.RIGHT,
    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: form.htbh,
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false,
          rowFlex: RowFlex.RIGHT
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 150,
      underline: true
    },
    controlId: 'housingtranContract.htbh'
  },
  { value: '\n', rowFlex: RowFlex.RIGHT },
  { value: '\n' },
  {
    value: ' \n',
    size: 44,
    bold: true,
    color: 'rgb(102, 102, 102)',
    italic: false,
    rowFlex: RowFlex.CENTER
  },
  { value: '\n' },
  {
    value: '',
    type: ElementType.TITLE,
    level: TitleLevel.FIRST, //标题级别
    valueList: [
      {
        value: '商品房买卖合同(预售)',
        size: 44,
        bold: true,
        color: 'rgb(102, 102, 102)',
        italic: false,
        rowFlex: RowFlex.CENTER
      }
    ]
  },
  { value: '\n' },
  {
    value: ' ',
    size: 16,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: ' ',
    size: 16,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  { value: '\n' },
  {
    value: ' ',
    size: 16,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: ' ',
    size: 16,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: ' ',
    size: 16,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n', rowFlex: RowFlex.CENTER },
  {
    value: '出卖人：',
    size: 24,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false,
    rowFlex: RowFlex.CENTER
  },
  {
    value: '',
    type: ElementType.CONTROL,
    rowFlex: RowFlex.CENTER,
    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: form.cmr,
          type: ElementType.TEXT,
          size: 24,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false,
          rowFlex: RowFlex.CENTER
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 300,
      underline: true
    },
    controlId: 'housingtranContract.cmr'
  },
  { value: '\n', rowFlex: RowFlex.CENTER },
  {
    value: '买受人：',
    size: 24,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false,
    rowFlex: RowFlex.CENTER
  },
  {
    value: '',
    type: ElementType.CONTROL,
    rowFlex: RowFlex.CENTER,
    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: form.msr,
          type: ElementType.TEXT,
          size: 24,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false,
          rowFlex: RowFlex.CENTER
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 300,
      underline: true
    },
    controlId: 'housingtranContract.msr'
  },
  { value: '\n' },
  { value: '\n' },
  {
    value: ' \n',
    size: 16,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n', rowFlex: RowFlex.CENTER },
  {
    value: '中华人民共和国住房和城乡建设部',
    size: 24,
    bold: true,
    color: 'rgb(102, 102, 102)',
    italic: false,
    rowFlex: RowFlex.CENTER
  },
  { value: '\n', bold: true, rowFlex: RowFlex.CENTER },
  {
    value:
      '                                                                                        ',
    size: 16,
    bold: true,
    color: 'rgb(102, 102, 102)',
    italic: false,
    rowFlex: RowFlex.CENTER
  },
  {
    value: '制定\n中华人民共和国国家工商行政管理总局\n',
    size: 24,
    bold: true,
    color: 'rgb(102, 102, 102)',
    italic: false,
    rowFlex: RowFlex.CENTER
  },
  {
    value: '\n',
    size: 24,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false,
    rowFlex: RowFlex.CENTER
  },
  {
    value: '二0一四年四月',
    size: 24,
    bold: true,
    color: 'rgb(102, 102, 102)',
    italic: false,
    rowFlex: RowFlex.CENTER
  },
  { value: '\n\n\n\n\n' },
  {
    value: '',
    type: ElementType.TITLE,
    level: TitleLevel.FIRST, //标题级别
    valueList: [
      {
        value: '目录\n',
        size: 26,
        bold: true,
        color: 'rgb(102, 102, 102)',
        italic: false,
        rowFlex: RowFlex.CENTER
      }
    ]
  },
  { value: '\n' },
  {
    value: '说明\n',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '专业术语解释\n',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '第一章 合同当事人\n',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '第二章 商品房基本状况\n',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '第三章 商品房价款\n',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '第四章 商品房交付条件和交付手续\n',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '第五章 面积差异处理方式\n',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '第六章 规划设计变更\n',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '第七章 商品房质量及保修责任\n',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '第八章 合同备案与房屋登记\n',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '第九章 前期物业管理\n',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '第十章 其他事项',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n\n' },

  {
    value: '',
    type: ElementType.TITLE,
    level: TitleLevel.SECOND, //标题级别
    valueList: [
      {
        value: '\n说 明',
        size: 26,
        bold: true,
        color: 'rgb(102, 102, 102)',
        italic: false,
        rowFlex: RowFlex.CENTER
      }
    ]
  },
  { value: '\n' },
  {
    value:
      '1.本合同文本为示范文本，由中华人民共和国住房和城乡建设部、中华人民共和国国家工商行政管理总局共同制定。各地可在有关法律法规、规定的范围内，结合实际情况调整合同相应内容。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value:
      '2.签订本合同前，出卖人应当向买受人出示《商品房预售许可证》及其他有关证书和证明文件。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value:
      '3.出卖人应当就合同重大事项对买受人尽到提示义务。买受人应当审慎签订合同，在签订本合同前，要仔细阅读合同条款，特别是审阅其中具有选择性、补充性、修改性的内容，注意防范潜在的市场风险和交易风险。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value:
      '4.本合同文本【】中选择内容、空格部位填写内容及其他需要删除或添加的内容，双方当事人应当协商确定【】中选择内容，以划√方式选定；对于实际情况未发生或双方当事人不作约定时，应当在空格部位打×，以示删除。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value:
      '5.出卖人与买受人可以针对本合同文本中没有约定或者约定不明确的内容，根据所售项目的具体情况在相关条款后的空白行中进行补充约定，也可以另行签订补充协议。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value:
      '6.双方当事人可以根据实际情况决定本合同原件的份数，并在签订合同时认真核对，以确保各份合同内容一致；在任何情况下，出卖人和买受人都应当至少持有一份合同原件。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n\n\n\n\n\n' },
  {
    value: '',
    type: ElementType.TITLE,
    level: TitleLevel.SECOND, //标题级别
    valueList: [
      {
        value: '\n专业术语解释',
        size: 26,
        bold: true,
        color: 'rgb(102, 102, 102)',
        italic: false,
        rowFlex: RowFlex.CENTER
      }
    ]
  },
  { value: '\n' },
  {
    value:
      '1.商品房预售：是指房地产开发企业将正在建设中的取得《商品房预售许可证》的商品房预先出售给买受人，并由买受人支付定金或房价款的行为。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '2.法定代理人：是指依照法律规定直接取得代理权的人。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value:
      '3.套内建筑面积：是指成套房屋的套内建筑面积，由套内使用面积、套内墙体面积、套内阳台建筑面积三部分组成。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value:
      '4.房屋的建筑面积：是指房屋外墙（柱）勒脚以上各层的外围水平投影面积，包括阳台、挑廊、地下室、室外楼梯等，且具备有上盖，结构牢固，层高2.20M 以上（含2.20M）的永久性建筑。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '5.不可抗力：是指不能预见、不能避免并不能克服的客观情况。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value:
      '6.民用建筑节能：是指在保证民用建筑使用功能和室内热环境质量的前提下，降低其使用过程中能源消耗的活动。民用建筑是指居住建筑、国家机关办公建筑和商业、服务业、教育、卫生等其他公共建筑。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value:
      '7.房屋登记：是指房屋登记机构依法将房屋权利和其他应当记载的事项在房屋登记簿上予以记载的行为。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value:
      '8.所有权转移登记：是指商品房所有权从出卖人转移至买受人所办理的登记类型。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value:
      '9.房屋登记机构：是指直辖市、市、县人民政府建设（房地产）主管部门或者其设置的负责房屋登记工作的机构。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value:
      '10.分割拆零销售：是指房地产开发企业将成套的商品住宅分割为数部分分别出售给买受人的销售方式。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value:
      '11.返本销售：是指房地产开发企业以定期向买受人返还购房款的方式销售商品房的行为。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value:
      '12.售后包租：是指房地产开发企业以在一定期限内承租或者代为出租买受人所购该企业商品房的方式销售商品房的行为。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: ' ',
    size: 16,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '\n\n\n商品房买卖合同',
    size: 26,
    bold: true,
    color: 'rgb(102, 102, 102)',
    italic: false,
    rowFlex: RowFlex.CENTER
  },
  { value: '\n' },
  {
    value: '（预售）',
    size: 24,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false,
    rowFlex: RowFlex.CENTER
  },
  { value: '\n\n' },
  {
    value:
      '出卖人向买受人出售其开发建设的房屋，双方当事人应当在自愿、平等、公平及诚实信用的基础上，根据《中华人民共和国合同法》、《中华人民共和国物权法》、《中华人民共和国城市房地产管理法》等法律、法规的规定，就商品房买卖相关内容协商达成一致意见，签订本商品房买卖合同。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '',
    type: ElementType.TITLE,
    level: TitleLevel.SECOND, //标题级别
    valueList: [
      {
        value: '第一章 合同当事人',
        size: 18,
        bold: true,
        color: 'rgb(102, 102, 102)',
        italic: false,
        rowFlex: RowFlex.CENTER
      },
    ]
  },
  { value: '\n' },
  {
    value: '',
    type: ElementType.CONTROL,
    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 1,
      underline: false
    },
    controlId: 'seller-start'
  },
  // { value: '出卖人：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
  // {
  //   value: '',
  //   type: ElementType.CONTROL,
  //   control: {
  //     conceptId: '4',
  //     type: ElementType.TEXT,
  //     value: [{ value: '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
  //     placeholder: '',
  //     prefix: '‌',
  //     postfix: '‌',
  //     minWidth: 200,
  //     underline: true
  //   }
  // },
  // { value: '\n' },
  // { value: '通讯地址：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
  // {
  //   value: '',
  //   type: ElementType.CONTROL,
  //   control: {
  //     conceptId: '4',
  //     type: ElementType.TEXT,
  //     value: [{ value: '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
  //     placeholder: '',
  //     prefix: '‌',
  //     postfix: '‌',
  //     minWidth: 200,
  //     underline: true
  //   }
  // },
  // { value: '\n' },
  // { value: '营业执照注册号：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
  // {
  //   value: '',
  //   type: ElementType.CONTROL,
  //   control: {
  //     conceptId: '4',
  //     type: ElementType.TEXT,
  //     value: [{ value: '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
  //     placeholder: '',
  //     prefix: '‌',
  //     postfix: '‌',
  //     minWidth: 200,
  //     underline: true
  //   }
  // },
  // { value: '\n' },
  // { value: '企业资质证书号：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
  // {
  //   value: '',
  //   type: ElementType.CONTROL,
  //   control: {
  //     conceptId: '4',
  //     type: ElementType.TEXT,
  //     value: [{ value: '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
  //     placeholder: '',
  //     prefix: '‌',
  //     postfix: '‌',
  //     minWidth: 200,
  //     underline: true
  //   }
  // },
  // { value: '\n' },
  // { value: '法定代表人：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
  // {
  //   value: '',
  //   type: ElementType.CONTROL,
  //   control: {
  //     conceptId: '4',
  //     type: ElementType.TEXT,
  //     value: [{ value: '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
  //     placeholder: '',
  //     prefix: '‌',
  //     postfix: '‌',
  //     minWidth: 100,
  //     underline: true
  //   }
  // },
  // { value: '联系电话：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
  // {
  //   value: '',
  //   type: ElementType.CONTROL,
  //   control: {
  //     conceptId: '4',
  //     type: ElementType.TEXT,
  //     value: [{ value: '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
  //     placeholder: '',
  //     prefix: '‌',
  //     postfix: '‌',
  //     minWidth: 100,
  //     underline: true
  //   }
  // },
  // { value: '\n' },
  // { value: '委托代理人：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
  // {
  //   value: '',
  //   type: ElementType.CONTROL,
  //   control: {
  //     conceptId: '4',
  //     type: ElementType.TEXT,
  //     value: [{ value: '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
  //     placeholder: '',
  //     prefix: '‌',
  //     postfix: '‌',
  //     minWidth: 100,
  //     underline: true
  //   }
  // },
  // { value: '联系电话：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
  // {
  //   value: '',
  //   type: ElementType.CONTROL,
  //   control: {
  //     conceptId: '4',
  //     type: ElementType.TEXT,
  //     value: [{ value: '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
  //     placeholder: '',
  //     prefix: '‌',
  //     postfix: '‌',
  //     minWidth: 100,
  //     underline: true
  //   }
  // },
  // { value: '\n' },
  // { value: '委托销售经纪机构：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
  // {
  //   value: '',
  //   type: ElementType.CONTROL,
  //   control: {
  //     conceptId: '4',
  //     type: ElementType.TEXT,
  //     value: [{ value: '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
  //     placeholder: '',
  //     prefix: '‌',
  //     postfix: '‌',
  //     minWidth: 200,
  //     underline: true
  //   }
  // },
  // { value: '\n' },
  // { value: '通讯地址：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
  // {
  //   value: '',
  //   type: ElementType.CONTROL,
  //   control: {
  //     conceptId: '4',
  //     type: ElementType.TEXT,
  //     value: [{ value: '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
  //     placeholder: '',
  //     prefix: '‌',
  //     postfix: '‌',
  //     minWidth: 200,
  //     underline: true
  //   }
  // },
  // { value: '\n' },
  // { value: '营业执照注册号：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
  // {
  //   value: '',
  //   type: ElementType.CONTROL,
  //   control: {
  //     conceptId: '4',
  //     type: ElementType.TEXT,
  //     value: [{ value: '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
  //     placeholder: '',
  //     prefix: '‌',
  //     postfix: '‌',
  //     minWidth: 200,
  //     underline: true
  //   }
  // },
  // { value: '\n' },
  // { value: '经济机构备案证明号：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
  // {
  //   value: '',
  //   type: ElementType.CONTROL,
  //   control: {
  //     conceptId: '4',
  //     type: ElementType.TEXT,
  //     value: [{ value: '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
  //     placeholder: '',
  //     prefix: '‌',
  //     postfix: '‌',
  //     minWidth: 200,
  //     underline: true
  //   }
  // },
  // { value: '\n' },
  // { value: '法定代表人：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
  // {
  //   value: '',
  //   type: ElementType.CONTROL,
  //   control: {
  //     conceptId: '4',
  //     type: ElementType.TEXT,
  //     value: [{ value: '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
  //     placeholder: '',
  //     prefix: '‌',
  //     postfix: '‌',
  //     minWidth: 100,
  //     underline: true
  //   }
  // },
  // { value: '联系电话：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
  // {
  //   value: '',
  //   type: ElementType.CONTROL,
  //   control: {
  //     conceptId: '4',
  //     type: ElementType.TEXT,
  //     value: [{ value: '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
  //     placeholder: '',
  //     prefix: '‌',
  //     postfix: '‌',
  //     minWidth: 100,
  //     underline: true
  //   }
  // },
  { value: '\n' },
  {
    value: ' ',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '',
    type: ElementType.CONTROL,
    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 1,
      underline: false
    },
    controlId: 'buyer-start'
  },
  // { value: '买受人：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
  // {
  //   value: '',
  //   type: ElementType.CONTROL,
  //   control: {
  //     conceptId: '4',
  //     type: ElementType.TEXT,
  //     value: [{ value: '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
  //     placeholder: '',
  //     prefix: '‌',
  //     postfix: '‌',
  //     minWidth: 200,
  //     underline: true
  //   }
  // },
  // { value: '\n' },
  // { value: '证件类型：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
  // {
  //   value: '',
  //   type: ElementType.CONTROL,
  //   control: {
  //     conceptId: '4',
  //     type: ElementType.TEXT,
  //     value: [{ value: '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
  //     placeholder: '',
  //     prefix: '‌',
  //     postfix: '‌',
  //     minWidth: 100,
  //     underline: true
  //   }
  // },
  // { value: '证号：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
  // {
  //   value: '',
  //   type: ElementType.CONTROL,
  //   control: {
  //     conceptId: '4',
  //     type: ElementType.TEXT,
  //     value: [{ value: '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
  //     placeholder: '',
  //     prefix: '‌',
  //     postfix: '‌',
  //     minWidth: 100,
  //     underline: true
  //   }
  // },
  // { value: '\n' },
  // { value: '出生日期：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
  // {
  //   value: '',
  //   type: ElementType.CONTROL,
  //   control: {
  //     conceptId: '4',
  //     type: ElementType.TEXT,
  //     value: [{ value: '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
  //     placeholder: '',
  //     prefix: '‌',
  //     postfix: '‌',
  //     minWidth: 100,
  //     underline: true
  //   }
  // },
  // { value: '', size: 13, bold: false, color: 'rgb(102, 102, 102)', italic: false },
  // { value: '性别：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
  // {
  //   value: '',
  //   type: ElementType.CONTROL,
  //   control: {
  //     conceptId: '4',
  //     type: ElementType.TEXT,
  //     value: [{ value: '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
  //     placeholder: '',
  //     prefix: '‌',
  //     postfix: '‌',
  //     minWidth: 100,
  //     underline: true
  //   }
  // },
  // { value: '\n' },
  // { value: '通讯地址：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
  // {
  //   value: '',
  //   type: ElementType.CONTROL,
  //   control: {
  //     conceptId: '4',
  //     type: ElementType.TEXT,
  //     value: [{ value: '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
  //     placeholder: '',
  //     prefix: '‌',
  //     postfix: '‌',
  //     minWidth: 200,
  //     underline: true
  //   }
  // },
  // { value: '\n' },
  // { value: '邮政编码：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
  // {
  //   value: '',
  //   type: ElementType.CONTROL,
  //   control: {
  //     conceptId: '4',
  //     type: ElementType.TEXT,
  //     value: [{ value: '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
  //     placeholder: '',
  //     prefix: '‌',
  //     postfix: '‌',
  //     minWidth: 100,
  //     underline: true
  //   }
  // },
  // { value: '联系电话：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
  // {
  //   value: '',
  //   type: ElementType.CONTROL,
  //   control: {
  //     conceptId: '4',
  //     type: ElementType.TEXT,
  //     value: [{ value: '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
  //     placeholder: '',
  //     prefix: '‌',
  //     postfix: '‌',
  //     minWidth: 100,
  //     underline: true
  //   }
  // },
  // { value: '\n' },
  // { value: '共有方式：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
  // {
  //   value: '',
  //   type: ElementType.CONTROL,
  //   control: {
  //     conceptId: '4',
  //     type: ElementType.TEXT,
  //     value: [{ value: '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
  //     placeholder: '',
  //     prefix: '‌',
  //     postfix: '‌',
  //     minWidth: 200,
  //     underline: true
  //   }
  // },
  // { value: '\n' },
  // { value: '份额：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
  // {
  //   value: '',
  //   type: ElementType.CONTROL,
  //   control: {
  //     conceptId: '4',
  //     type: ElementType.TEXT,
  //     value: [{ value: '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
  //     placeholder: '',
  //     prefix: '‌',
  //     postfix: '‌',
  //     minWidth: 200,
  //     underline: true
  //   }
  // },
  { value: '\n' },
  {
    value: '',
    type: ElementType.TITLE,
    level: TitleLevel.SECOND, //标题级别
    valueList: [
      {
        value: '第二章 商品房基本状况',
        size: 18,
        bold: true,
        color: 'rgb(102, 102, 102)',
        italic: false,
        rowFlex: RowFlex.CENTER
      },
    ]
  },
  { value: '\n' },
  {
    value: '第一条',
    size: 18,
    bold: true,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: ' 项目建设依据',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '1.出卖人以',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    }
  },
  {
    value: '方式取得坐落于',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    },
    controlId: 'projectManagerInfo.tdzl'
  },
  {
    value: '地块的建设用地使用权。该地块【国有土地使用证号】为',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    }
  },
  {
    value: '，土地使用权面积为',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    },
    controlId: 'projectManagerInfo.tdsymj'
  },
  {
    value: '平方米。买受人购买的商品房（以下简称该商品房）所占用的土地用途为',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    },
    controlId: 'projectManagerInfo.tdyt'
  },
  {
    value: '，土地使用权终止日期为',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    },
    controlId: 'projectManagerInfo.syjssj'
  },
  { value: '\n' },
  {
    value: '2.出卖人经批准，在上述地块上建设的商品房项目核准名称为',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    },
    controlId: 'projectManagerInfo.xmmc'
  },
  {
    value: '，建设工程规划许可证号为',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    },
    controlId: 'projectManagerInfo.jsgcghxkzh'
  },
  {
    value: '，建筑工程施工许可证号为',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    },
    controlId: 'projectManagerInfo.jsgcsgxkzh'
  },
  {
    value: '。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '第二条',
    size: 18,
    bold: true,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: ' 预售依据',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '该商品房已由',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    }
  },
  {
    value: '预售批准，预售许可证号为',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    },
    controlId: 'housingtranContract.ysxkzh'
  },
  {
    value: '。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '第三条',
    size: 18,
    bold: true,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: ' 商品房的基本情况',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '1.该商品房的规划用途为',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    }
  },
  {
    value: '。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '2.该商品房所在建筑物的主体结构为',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    }
  },
  {
    value: '，建筑总层数为',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    }
  },
  {
    value: '层，其中地上',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    }
  },
  {
    value: '层，地下',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    }
  },
  {
    value: '层。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '3.该商品房为第一条规定项目中的',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    }
  },
  {
    value: '单元',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    }
  },
  {
    value: '层 ',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      underline: true
    }
  },
  {
    value:
      '号。房屋竣工后，如房号发生改变，不影响该商品房的特定位置。该商品房的平面图见附件一。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '4.该商品房的房产测绘机构为',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    }
  },
  {
    value: '，其预测建筑面积共',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    }
  },
  {
    value: '平方米，其中套内建筑面积',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '   ',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false,
    underline: true
  },
  {
    value: '平方米，分摊共有建筑面积',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '   ',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false,
    underline: true
  },
  {
    value: '平方米。该商品房共用部件见附件二。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '该商品房层高为',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    }
  },
  {
    value: '米，有',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    }
  },
  {
    value: '个阳台，其中',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    }
  },
  {
    value: '个阳台为封闭式，',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    }
  },
  {
    value: '个阳台为非封闭式。阳台是否封闭以规划设计文件为准。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '第四条',
    size: 18,
    bold: true,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: ' 抵押情况',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '与该商品房有关的抵押情况为【 】。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '抵押类型：',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      underline: true
    }
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    }
  },
  {
    value: '，抵押人：',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    }
  },
  {
    value: '，',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '抵押权人：',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    }
  },
  {
    value: '，抵押登记机构：',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    }
  },
  {
    value: '，',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '抵押登记日期：',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    }
  },
  {
    value: '，债务履行期限：',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    }
  },
  {
    value: '。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '抵押权人同意该商品房转让的证明及关于抵押的相关约定见附件三。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '第五条',
    size: 18,
    bold: true,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: ' 房屋权利状况承诺',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '1.出卖人对该商品房享有合法权利；',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '2.该商品房没有出售给除本合同买受人以外的其他人；',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '3.该商品房没有司法查封或其他限制转让的情况；',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: '4.',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 200,
      underline: true
    }
  },
  { value: '\n' },
  {
    value: '5.',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 200,
      underline: true
    }
  },
  { value: '\n' },
  {
    value:
      '如该商品房权利状况与上述情况不符，导致不能完成本合同登记备案或房屋所有权转移登记的，买受人有权解除合同。买受人解除合同的，应当书面通知出卖人。出卖人应当自解除合同通知送达之日起 15日内退还买受人已付全部房款（含已付贷款部分），并自买受人付款之日起，按照',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  {
    value: '',
    type: ElementType.CONTROL,

    control: {
      conceptId: '4',
      type: ControlType.TEXT,
      value: [
        {
          value: '',
          type: ElementType.TEXT,
          size: 17,
          bold: false,
          color: 'rgb(102, 102, 102)',
          italic: false
        }
      ],
      placeholder: '',
      prefix: '‌',
      postfix: '‌',
      minWidth: 50,
      underline: true
    }
  },
  {
    value:
      '%（不低于中国人民银行公布的同期贷款基准利率）计算给付利息。给买受人造成损失的，由出卖人支付【 】的赔偿金。',
    size: 18,
    bold: false,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' },
  {
    value: ' ',
    size: 18,
    bold: true,
    color: 'rgb(102, 102, 102)',
    italic: false
  },
  { value: '\n' }
]
// 出卖人和买受人需单独处理，在首次进入时，判断是否为空，为空则赋值，否则不赋值
const sellerArr: IElement[] = []
const sellerList = form.sellerList
if (sellerList && sellerList.length > 0) {
  sellerList.forEach(item => {
    sellerArr.push(
      { value: '出卖人：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
      {
        value: '',
        type: ElementType.CONTROL,
        control: {
          conceptId: '4',
          type: ControlType.TEXT,
          value: [{ value: item.qlrmc ? item.qlrmc : '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
          placeholder: '',
          prefix: '‌',
          postfix: '‌',
          minWidth: 200,
          underline: true
        }
      },
      { value: '\n' },
      { value: '社会信用统一代码：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
      {
        value: '',
        type: ElementType.CONTROL,
        control: {
          conceptId: '4',
          type: ControlType.TEXT,
          value: [{ value: item.zjh ? item.zjh : '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
          placeholder: '',
          prefix: '‌',
          postfix: '‌',
          minWidth: 200,
          underline: true
        }
      },
      { value: '\n' },
      { value: '注册地址：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
      {
        value: '',
        type: ElementType.CONTROL,
        control: {
          conceptId: '4',
          type: ControlType.TEXT,
          value: [{ value: item.dz ? item.dz : '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
          placeholder: '',
          prefix: '‌',
          postfix: '‌',
          minWidth: 200,
          underline: true
        }
      },
      { value: '\n' },
      { value: '企业资质证书号：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
      {
        value: '',
        type: ElementType.CONTROL,
        control: {
          conceptId: '4',
          type: ControlType.TEXT,
          value: [{ value: '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
          placeholder: '',
          prefix: '‌',
          postfix: '‌',
          minWidth: 200,
          underline: true
        }
      },
      { value: '\n' },
      { value: '法定代表人：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
      {
        value: '',
        type: ElementType.CONTROL,
        control: {
          conceptId: '4',
          type: ControlType.TEXT,
          value: [{ value: item.frmc ? item.frmc : '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
          placeholder: '',
          prefix: '‌',
          postfix: '‌',
          minWidth: 100,
          underline: true
        }
      },
      { value: ' 联系电话：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
      {
        value: '',
        type: ElementType.CONTROL,
        control: {
          conceptId: '4',
          type: ControlType.TEXT,
          value: [{ value: item.frdh ? item.frdh : '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
          placeholder: '',
          prefix: '‌',
          postfix: '‌',
          minWidth: 100,
          underline: true
        }
      },
      { value: '\n' }
    )
  })
}
const buyArr: IElement[] = []
const buyerList = form.buyerList
if (buyerList && buyerList.length > 0) {
  buyerList.forEach(item => {
    buyArr.push(
      { value: '买受人：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
      {
        value: '',
        type: ElementType.CONTROL,
        control: {
          conceptId: '4',
          type: ControlType.TEXT,
          value: [
            {
              value: item.qlrmc ? item.qlrmc : '',
              type: ElementType.TEXT,
              size: 17,
              bold: false,
              color: 'rgb(102, 102, 102)',
              italic: false
            }
          ],
          placeholder: '',
          prefix: '‌',
          postfix: '‌',
          minWidth: 200,
          underline: true
        }
      },
      { value: '\n' },
      { value: '证件类型：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
      {
        value: '',
        type: ElementType.CONTROL,
        control: {
          conceptId: '4',
          type: ControlType.TEXT,
          value: [
            {
              value: item.zjzl ? item.zjzl : '',
              type: ElementType.TEXT,
              size: 17,
              bold: false,
              color: 'rgb(102, 102, 102)',
              italic: false
            }
          ],
          placeholder: '',
          prefix: '‌',
          postfix: '‌',
          minWidth: 100,
          underline: true
        }
      },
      { value: '证号：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
      {
        value: '',
        type: ElementType.CONTROL,
        control: {
          conceptId: '4',
          type: ControlType.TEXT,
          value: [
            {
              value: item.zjh ? item.zjh : '',
              type: ElementType.TEXT,
              size: 17,
              bold: false,
              color: 'rgb(102, 102, 102)',
              italic: false
            }
          ],
          placeholder: '',
          prefix: '‌',
          postfix: '‌',
          minWidth: 100,
          underline: true
        }
      },
      { value: '\n' },
      { value: ' ', size: 13, bold: false, color: 'rgb(102, 102, 102)', italic: false },
      { value: '性别：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
      {
        value: '',
        type: ElementType.CONTROL,
        control: {
          conceptId: '4',
          type: ControlType.TEXT,
          value: [{ value: '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
          placeholder: '',
          prefix: '‌',
          postfix: '‌',
          minWidth: 100,
          underline: true
        }
      },
      { value: '\n' },
      { value: '通讯地址：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
      {
        value: '',
        type: ElementType.CONTROL,
        control: {
          conceptId: '4',
          type: ControlType.TEXT,
          value: [
            {
              value: item.dz ? item.dz : '',
              type: ElementType.TEXT,
              size: 17,
              bold: false,
              color: 'rgb(102, 102, 102)',
              italic: false
            }
          ],
          placeholder: '',
          prefix: '‌',
          postfix: '‌',
          minWidth: 200,
          underline: true
        }
      },
      { value: '\n' },
      { value: '邮政编码：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
      {
        value: '',
        type: ElementType.CONTROL,
        control: {
          conceptId: '4',
          type: ControlType.TEXT,
          value: [{ value: '', type: ElementType.TEXT, size: 17, bold: false, color: 'rgb(102, 102, 102)', italic: false }],
          placeholder: '',
          prefix: '‌',
          postfix: '‌',
          minWidth: 100,
          underline: true
        }
      },
      { value: '联系电话：', size: 18, bold: false, color: 'rgb(102, 102, 102)', italic: false },
      {
        value: '',
        type: ElementType.CONTROL,
        control: {
          conceptId: '4',
          type: ControlType.TEXT,
          value: [
            {
              value: item.dh ? item.dh : '',
              type: ElementType.TEXT,
              size: 17,
              bold: false,
              color: 'rgb(102, 102, 102)',
              italic: false
            }
          ],
          placeholder: '',
          prefix: '‌',
          postfix: '‌',
          minWidth: 100,
          underline: true
        }
      },
      { value: '\n' }
    )
  })
}
// 插入买受人数据
for (let i = 0; i < elementList.length; i++) {
  const item = elementList[i]
  if (item.controlId && item.controlId === 'seller-start') {
    elementList.splice(i + 1, 0, ...sellerArr)
  }
  if (item.controlId && item.controlId === 'buyer-start') {
    elementList.splice(i + 1, 0, ...buyArr)
  }
}
export const data: IElement[] = elementList

export const header: IElement[] = [
  // {
  //   value: '某某科技公司',
  //   size: 28,
  //   rowFlex: RowFlex.CENTER
  // },
  {
    value: '☆商品房买卖合同登记备案☆',
    size: 18,
    rowFlex: RowFlex.CENTER
  },
  // {
  //   value: '\n',
  //   type: ElementType.SEPARATOR
  // }
]

export const options: IEditorOption = {
  margins: [100, 120, 100, 120],
  watermark: {
    data: '测试文件',
    size: 120
  },
  pageNumber: {
    format: '第{pageNo}页/共{pageCount}页'
  },
  placeholder: {
    data: '请输入正文'
  },
  zone: {
    tipDisabled: false
  },
  maskMargin: [60, 0, 30, 0] // 菜单栏高度60，底部工具栏30为遮盖层
}
interface IComment {
  id: string
  content: string
  userName: string
  rangeText: string
  createdDate: string
}
export const commentList: IComment[] = []